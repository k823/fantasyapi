const app = document.getElementById('root')


const container = document.createElement('div')
container.setAttribute('class', 'container')

app.appendChild(container)

let peliculas;
const loader = document.querySelector('.loader-container');
const input = document.querySelector('#myInput')

function getFilms(){
    fetch('https://ghibliapi.herokuapp.com/films')
    .then(response => {
      return response.json()
    })
    .then(data => {
      console.log(data)
      peliculas = data;
      paint(data);
    })
    .catch(err => {
        const errorMessage = document.createElement('marquee')
        errorMessage.textContent = `Gah, it's not working!`
        app.appendChild(errorMessage)
    });
    return peliculas;
}
 
function paint(peliculas){
    peliculas.forEach(movie => {
        const card = document.createElement('div')
        card.setAttribute('class', 'card')

        const h1 = document.createElement('h1')
        h1.textContent = movie.title

        const p = document.createElement('p')
        movie.description = movie.description.substring(0, 300)
        p.textContent = `${movie.description}...`

        container.appendChild(card)
        card.appendChild(h1)
        card.appendChild(p)      
    })
    setTimeout(function() {
        loader.style.display = 'none';
    }, 2000)
}

function userInput(ev){
    let value;
    if( ev && ev.target && ev.target.value) {
        value = ev.target.value;
    } else {
        value = input.value;
    }
    loader.style.display = 'block';
   var filteredFilms = peliculas.filter(pelicula => pelicula.title.toLowerCase().includes(value));
    container.innerHTML = '';
    paint(filteredFilms);

}

document.getElementById('myInput').addEventListener('change', userInput)
document.getElementById('myButton').addEventListener('click', userInput)


getFilms();
   